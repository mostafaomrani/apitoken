
## Mostafa Omrani 
## About ApiToken 

------------------------------------

Web Interface Access  

Address : http://45.149.77.50/ 

-------------------------------

API Interface 

Register : 

Address : GET : http://45.149.77.50/api/register

Send Get Request And post (name,email,password) Parameter .

Parameter Keys :
              - Name (String) ,email (String) ,password (String)

Header keys :
                 Accept -> application/json   
                     Content-Type -> application/json 


-------------------------------------------------------------

Login : 

 Address : Post : http://45.149.77.50/api/login

Send Post  Request And post Correct (email and password) .
Return : API TOKEN 
   
Parameter Keys :
                 - Name (String) ,email (String)

Header keys :
              Accept -> application/json   
                 Content-Type -> application/json  


-------------------------------------------------------------

User : 

 Address : GET : http://45.149.77.50/api/user
 
Header keys :
              Accept -> application/json   
                 Content-Type -> application/json  
                 Authorization -> TOKEN 


-------------------------------------------------------------

Get Process List : 

 Address : GET : http://45.149.77.50/api/processlist
 
Header keys :
               Accept -> application/json   
                  Content-Type -> application/json  
                  Authorization -> TOKEN 


-------------------------------------------------------------

Get List of directories : 

 Address : GET : http://45.149.77.50/api/getdir
 
Header keys :
               Accept -> application/json   
                  Content-Type -> application/json  
                  Authorization -> TOKEN 



-------------------------------------------------------------

Get Files List : 

 Address : GET : http://45.149.77.50/api/getfiles
 
Header keys :
               Accept -> application/json   
                  Content-Type -> application/json  
                  Authorization -> TOKEN 

-------------------------------------------------------------

Make Directory : 

 Address : POST : http://45.149.77.50/api/storedirectory
 
Header keys :
               Accept -> application/json   
                  Content-Type -> application/json  
                  Authorization -> TOKEN 

Parameter Keys :
Name (String)




-------------------------------------------------------------

Make File : 

 Address : POST : http://45.149.77.50/api/storefile
 
Header keys :
               Accept -> application/json   
                  Content-Type -> application/json  
                  Authorization -> TOKEN 

Parameter Keys :
Name (String) ,
Data : (String) 