<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Requests\FileRequest;
use Illuminate\Support\Facades\File;
use App\Http\Requests\DirectoryRequest;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        return view('home');
    }



    public function processList()
    {
        $list = [];
        for ($i = 2; $i <= 20; $i++) {
            $list []=   exec('ps -aef | head -' . $i);
        }
        return view('list', compact('list'));
    }


    public function directortCreate()
    {
        return view('createdirectory');
    }


    public function directortStore(DirectoryRequest $request)
    {
        mkdir('/opt/myprogram/' . $request->name, 0777, true);
        Session::flash('status', 'Directory created successfully.');
        return redirect(route('directory.create'));
    }



    public function fileCreate()
    {
        return view('createfile');
    }

    public function fileStore(FileRequest $request)
    {
        try {
            File::put('/opt/myprogram/Files/' . $request->name . '.txt', $request->data);
            Session::flash('status', 'File created successfully In Files directory.');
            return redirect(route('file.create'));
        } catch (Exception $ex) {
            // IF Foler Files directory not exist Crate Directory first and create new File;
            mkdir('/opt/myprogram/Files', 0777, true);
            File::put('/opt/myprogram/Files/' . $request->name . '.txt', $request->data);
            Session::flash('status', 'File created successfully In Files directory.');
            return redirect(route('file.create'));
        }
    }



    public function getDir()
    {
        $list = array_filter(glob('/opt/myprogram/*'), 'is_dir');
        return view('list', compact('list'));
    }

    public function getFiles()
    {
        $list = array_filter(glob('/opt/myprogram/*'), 'is_file');
        return view('list', compact('list'));
    }
}
