<?php

namespace App\Http\Controllers\Api;

use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests\FileRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\DirectoryRequest;

class ApiController extends Controller
{

    /*
   * Get  Top 18 Running Process
   * @return Json
   * */
    public function processList()
    {
        $list = [];
        for ($i = 2; $i <= 20; $i++) {
            $list[] =   exec('ps -aef | head -' . $i);
        }
        return response()->json($list);
    }


    /*
   * Get  Top 18 Running Process
   * @return Json
   * */
    public function directortStore(DirectoryRequest $request)
    {
        mkdir('/opt/myprogram/' . $request->name, 0777, true);
        return response()->json([
            "message" => "Directory Create :)"
        ]);
    }



    /*
   * Get  Top 18 Running Process
   * @return Json
   * */
  public function fileStore(FileRequest $request)
  {
      try {
          File::put('/opt/myprogram/Files/' . $request->name . '.txt', $request->data);
          return response()->json([
            "message" => "File successfully Create In Files Directory :)"
        ]);
      } catch (Exception $ex) {
          // IF Foler Files directory not exist Crate Directory first and create new File;
          mkdir('/opt/myprogram/Files', 0777, true);
          File::put('/opt/myprogram/Files/' . $request->name . '.txt', $request->data);
          return response()->json([
            "message" => "File successfully Create In Files Directory :)"
        ]);
      }
  }



    /*
   *  return list of directory
   * @return Json
   * */
    public function getDir()
    {
        $list = array_filter(glob('/opt/myprogram/*'), 'is_dir');
        return response()->json($list);
    }



    /*
   * return list of files
   * @return Json
   * */
    public function getFiles()
    {
        $list = array_filter(glob('/opt/myprogram/Files/*'), 'is_file');
        return response()->json($list);
    }
}
