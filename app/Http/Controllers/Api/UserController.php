<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

     /*
    * 
    * Return User Token Get 
    */

    public function login(LoginRequest $request)
    {
        $user = $this->user::whereEmail($request->email)->first();
        if (!$user || !Hash::check($request->password, $user->password)) {
            return response([
                'email' => ['The provided credentials are incorrect.'],
            ], 404);
        }
        return $user->createToken('ParsPackToken')->plainTextToken;
    }


    /*
    * 
    * Return Json User Instance  200 Status responce
    */

    public function register(RegisterRequest $request, User $user)
    {
        $this->user->name = $request->name;
        $this->user->email = $request->email;
        $this->user->password = Hash::make($request->password);

        if ($this->user->save() && $this->user instanceof User) {

            mkdir('/opt/myprogram/' . $this->user->name, 0777, true);
            File::put('/opt/myprogram/' . $this->user->name . '.txt', 'Mostafa Omrani');
            return response()->json([
                'message' => 'User create :)',
                'status' => 200
            ]);
            
        } else {
            return response()->json([
                'message' => 'User create :)',
                'status' => 401
            ]);
        }
    }


    /*
    * @api 
      Return User Information  
    *@ Return json
    */

    public function user(Request $request)
    {
        return response()->json([
            $request->user()
        ]);
    }
}
