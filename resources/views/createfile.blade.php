@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard - Create Directory</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form method="post" action="{{route('file.store')}}">
                        @csrf
                        <div class="form-group">
                            <label>File Name : </label>
                            <input required class="form-control" name="name" placeholder="Enter File Name">
                        </div>


                        <div class="form-group">
                            <label>File Data : </label>
                            <textarea required class="form-control" name="data" placeholder="Enter File Data"></textarea>
                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
