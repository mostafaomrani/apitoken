@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul>

                        <li><a href="{{route('directory.create')}}">Make Directory</a></li>
                        <li><a href="{{route('file.create')}}">Make File</a></li>
                        <hr>
                        <li><a href="{{route('processlist')}}">Show process list</a></li>
                        <hr>
                        <li><a href="{{route('getdir')}}">List of directory</a></li>
                        <li><a href="{{route('getfiles')}}">List of File in this directory</a></li>
          
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
