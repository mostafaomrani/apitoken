<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'auth:sanctum'], function () {
    
    Route::get('/user', 'Api\UserController@user');
    Route::post('/storedirectory', 'Api\ApiController@directortStore');
    Route::post('/storefile', 'Api\ApiController@fileStore');
    Route::get('/processlist', 'Api\ApiController@processList');
    Route::get('/getdir', 'Api\ApiController@getDir');
    Route::get('/getd', 'Api\ApiController@getDir');
    Route::get('/getfiles', 'Api\ApiController@getFiles');

});

Route::post('/login', 'Api\UserController@login');
Route::get('/register', 'Api\UserController@register');