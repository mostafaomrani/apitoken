<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/directory/create', 'HomeController@directortCreate')->name('directory.create');
    Route::post('/directory/store', 'HomeController@directortStore')->name('directory.store');
    Route::get('/file/create/', 'HomeController@fileCreate')->name('file.create');
    Route::post('/file/store', 'HomeController@fileStore')->name('file.store');
    Route::get('/processlist', 'HomeController@processList')->name('processlist');
    Route::get('/getdir', 'HomeController@getDir')->name('getdir');
    Route::get('/getfiles', 'HomeController@getFiles')->name('getfiles');
});
